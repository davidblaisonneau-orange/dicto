#!/usr/bin/env python3
"""DictO object."""


class DictO(dict):
    """
    Enhance a dict with attribut accessor.

    :Example:
    >>> d = {'a': 1, 'b': {'c': 2}, 'd': ["hi", {'foo': "bar"}]}
    >>> c=DictO(d)
    >>> c['a']
    1
    >>> c.a
    1
    >>> c['b.c']
    2
    """

    cast_type = [dict]

    def __init__(self, data, cast_type=None):
        """
        Init DictO class.

        :param data: The dict like to create
        :type key: dict
        :param cast_type: The list of type to cast as dictO
        :type key: list
        :return: a Dicto Class
        :rtype: DictO
        """
        if cast_type is not None:
            self.__set_cast_type__(cast_type)
        super(DictO, self).__init__(data)

    def __get_cast_type__(self):
        """
        Get the list of type to cast as dictO.

        :return: list of type to cast as dictO
        :rtype: list
        """
        return self.__class__.cast_type

    def __set_cast_type__(self, cast_type):
        """
        Set the list of type to cast as dictO.

        :param cast_type: The list of type to cast as dictO
        :type key: list
        """
        # pylint: disable=C0123
        if type(cast_type) is not list:
            cast_type = [cast_type]
        if dict not in cast_type:
            cast_type.append(dict)
        self.__class__.cast_type = cast_type

    def __getattr__(self, key):
        """
        Get object attribute.

        :param key: key of the attribute to get
        :type key: string
        :return: the object requested
        :rtype: DictO object

        :Example:
        >>> d = {'a': 1, 'b': {'c': 2}, 'd': ["hi", {'foo': "bar"}]}
        >>> c = DictO(d)
        >>> c['a']
        1
        >>> c.a
        1
        >>> c.b
        {'c': 2}
        >>> c.b.c
        2
        """
        if key in self:
            value = self[key]
            if isinstance(value, dict):
                # pylint: disable=C0123
                if type(value) in self.__class__.cast_type:
                    return DictO(value, self.__class__.cast_type)
                return value
            if isinstance(value, list):
                # pylint: disable=C0123
                return [DictO(x, self.__class__.cast_type)
                        if type(x) in self.__class__.cast_type else x
                        for x in value]
            return value
        raise AttributeError("No such attribute: " + key)

    def __getitem__(self, key):
        """
        Get dict key or git subkeys.

        :param key: key of the attribute to get
        :type key: string
        :return: the object requested
        :rtype: DictO object

        :Example:
        >>> d = {'a': 1, 'b': {'c': 2}, 'd': ["hi", {'foo': "bar"}]}
        >>> c = DictO(d)
        >>> c['a']
        1
        >>> c['b.c']
        2
        >>> c['d.1.foo']
        'bar'
        """
        if '.' in key:
            keys = key.split('.')
            value = dict.__getitem__(self, keys[0])
            if isinstance(value, dict):
                # pylint: disable=C0123
                if type(value) in self.__class__.cast_type:
                    return DictO(value, self.__class__.cast_type)[
                        '.'.join(keys[1:])]
                return value['.'.join(keys[1:])]
            if isinstance(value, list):
                subv = value[int(keys[1])]
                # pylint: disable=C0123
                if type(subv) in self.__class__.cast_type:
                    return DictO(subv, self.__class__.cast_type)[
                        '.'.join(keys[2:])]
                return subv
            return getattr(value, '.'.join(keys[1:]))
        return dict.__getitem__(self, key)

    def __setattr__(self, key, value):
        """
        Set a new object attribute.

        :param key: key of the attribute to set
        :type key: string
        :param value: value of the attribute to set
        :type key: any

        :Example:
        >>> d = {'a': 1, 'b': {'c': 2}, 'd': ["hi", {'foo': "bar"}]}
        >>> c = DictO(d)
        >>> c.e = 5
        >>> c.e
        5
        """
        self[key] = value

    def setrecurive(self, key, value):
        """
        Set a new object attribute with recursivity.

        :param key: key of the attribute to set
        :type key: string
        :param value: value of the attribute to set
        :type key: any

        :Example:
        >>> c = DictO({})
        >>> c.setrecurive('a.b.c.d', 5)
        >>> c.a.b.c.d
        5
        """
        if '.' in key:
            val = value
            keys = key.split('.')
            for k in reversed(keys):
                val = {k: val}
            self.update(val)
        else:
            self[key] = value

    def __delattr__(self, key):
        """
        Delete an object attribute.

        :param key: key of the attribute to del
        :type key: string

        :Example:
        >>> d = {'a': 1, 'b': {'c': 2}, 'd': ["hi", {'foo': "bar"}]}
        >>> c = DictO(d)
        >>> del(c.d)
        >>> c
        {'a': 1, 'b': {'c': 2}}
        """
        if key in self:
            del self[key]
        else:
            raise AttributeError("No such attribute: " + key)

    def __str__(self):
        """
        Print a nice structure.

        :Example:
        >>> d = {'a': 1, 'b': {'c': 2}, 'd': ["hi", {'foo': "bar"}]}
        >>> c = DictO(d)
        >>> c.__str__()
        "{'a': 1, 'b': {'c': 2}, 'd': ['hi', {'foo': 'bar'}]}"
        """
        from pprint import pformat
        return pformat(self)

    def __contains__(self, key):
        """
        Test if an attribute exists.

        :param key: key of the attribute to del
        :type key: string

        :Example:
        >>> d = {'a': 1, 'b': {'c': 2}, 'd': ["hi", {'foo': "bar"}]}
        >>> c = DictO(d)
        >>> 'a' in c
        True
        >>> 'b.c' in c
        True
        >>> 'x' in c
        False
        """
        if '.' in key:
            keys = key.split('.')
            if keys[0] in self:
                value = dict.__getitem__(self, keys[0])
                if isinstance(value, dict):
                    return '.'.join(keys[1:]) in DictO(value)
                if isinstance(value, list):
                    if int(keys[1]) < len(value):
                        if len(keys) == 2:
                            return True
                        subv = value[int(keys[1])]
                        if isinstance(subv, dict):
                            return '.'.join(keys[2:]) in DictO(subv)
                        raise AttributeError("Too many list depth")
                    return False
            return False
        return dict.__contains__(self, key)

#
# if __name__ == "__main__":
#     import doctest
#     doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE,
#                     raise_on_error=True)
