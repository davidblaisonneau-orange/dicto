#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from dicto import DictO
import pytest

"""
DictO unit tests
"""

original_dict = {'a': 1,
                 'b': {'c': 2},
                 'd': ["hi",
                       {'foo': "bar"}]}


class DictOInherit(DictO):

    def __init__(self, name):
        self.name = name


class MySimpleObj:

    def __init__(self):
        self.key = {'first': 'something'}


obj1 = DictOInherit('john')
obj2 = DictOInherit('jane')
obj3 = MySimpleObj()

dict_with_obj = {'players': [obj1, obj2],
                 'roles': {
                    'king': obj1,
                    'queen': obj2
                    },
                 'master': obj2,
                 'reward': obj3
                 }


def test_init():
    d = DictO(original_dict)
    assert type(DictO(d)) == DictO


def test_repr():
    obj = DictO(original_dict)
    assert obj == {'a': 1, 'b': {'c': 2}, 'd': ['hi', {'foo': 'bar'}]}


def test_print():
    obj = DictO(original_dict)
    assert obj.__str__() == "{'a': 1, 'b': {'c': 2}, 'd': ['hi', {'foo': 'bar'}]}"


def test_get_a_like_dict():
    obj = DictO(original_dict)
    assert obj['a'] == 1


def test_get_a_like_obj():
    obj = DictO(original_dict)
    assert obj.a == 1


def test_get_b_like_dict():
    obj = DictO(original_dict)
    assert obj['b'] == {'c': 2}


def test_get_b_like_obj():
    obj = DictO(original_dict)
    assert obj.b == {'c': 2}


def test_get_b_c_like_dict():
    obj = DictO(original_dict)
    assert obj['b']['c'] == 2


def test_get_b_c_like_obj():
    obj = DictO(original_dict)
    assert obj.b.c == 2


def test_get_d_like_dict():
    obj = DictO(original_dict)
    assert obj['d'] == ["hi", {'foo': "bar"}]


def test_get_d_like_obj():
    obj = DictO(original_dict)
    assert obj.d == ["hi", {'foo': "bar"}]


def test_get_d0_like_dict():
    obj = DictO(original_dict)
    assert obj['d.0'] == "hi"


def test_get_d1Foo_like_dict():
    obj = DictO(original_dict)
    assert obj['d'][1]['foo'] == "bar"


def test_get_d1foo_like_obj():
    obj = DictO(original_dict)
    assert obj.d[1].foo == "bar"


def test_get_keys():
    obj = DictO(original_dict)
    assert list(obj.keys()) == ['a', 'b', 'd']


def test_gel_fail():
    obj = DictO(original_dict)
    with pytest.raises(AttributeError) as excinfo:
        obj.e


def test_in_True():
    obj = DictO(original_dict)
    assert ('b' in obj) is True


def test_in_True_complex():
    obj = DictO(original_dict)
    assert ('b.c' in obj) is True


def test_in_True_complex_with_list():
    obj = DictO(original_dict)
    assert ('d.0' in obj) is True


def test_in_True_complex_with_dict_in_list():
    obj = DictO(original_dict)
    assert ('d.1.foo' in obj) is True


def test_in_False_simple():
    obj = DictO(original_dict)
    assert ('x' in obj) is False


def test_in_False_complex_bad_first_key():
    obj = DictO(original_dict)
    assert ('x.y' in obj) is False


def test_in_False_complex_bad_second_value():
    obj = DictO(original_dict)
    assert ('b.y' in obj) is False


def test_in_False_complex_bad_list_index():
    obj = DictO(original_dict)
    assert ('d.5' in obj) is False


def test_in_False_complex_bad_dict_key_in_list():
    obj = DictO(original_dict)
    assert ('d.1.nope' in obj) is False


def test_in_False_complex_list_in_list():
    obj = DictO({'a': ['hi', [1, 2]]})
    with pytest.raises(AttributeError):
        'a.1.0' in obj


def test_set():
    obj = DictO(original_dict)
    obj.e = 5
    assert obj.e == 5


def test_set_as_item():
    obj = DictO(original_dict)
    obj['e'] = 5
    assert obj.e == 5


def test_setrecurive_simple():
    obj = DictO(original_dict)
    obj.setrecurive('a', 5)
    assert obj.a == 5


def test_setrecurive():
    obj = DictO(original_dict)
    obj.setrecurive('r.e.c.u.r.s.e', 5)
    assert obj.r.e.c.u.r.s.e == 5


def test_del():
    obj = DictO(original_dict)
    del(obj.d)
    assert obj == {'a': 1, 'b': {'c': 2}}


def test_del_fail():
    obj = DictO(original_dict)
    with pytest.raises(AttributeError) as excinfo:
        del(obj.e)


def test_subkey_1():
    obj = DictO(original_dict)
    assert obj['b.c'] == 2


def test_subkey_2():
    obj = DictO(original_dict)
    assert obj['d.1.foo'] == "bar"


def test_obj():
    obj = DictO(dict_with_obj)
    assert obj.master.name == 'jane'


def test_obj_like_dict():
    obj = DictO(dict_with_obj)
    assert obj['master.name'] == 'jane'


def test_obj_in_list_type():
    obj = DictO(dict_with_obj)
    assert type(obj.players[0]) == DictOInherit


def test_obj_in_list_attribute():
    obj = DictO(dict_with_obj)
    assert obj.players[0].name == 'john'


def test_non_dict_obj():
    obj = DictO(dict_with_obj)
    assert obj.reward.key == {'first': 'something'}


def test_non_dict_obj_subvalue():
    obj = DictO(dict_with_obj)
    assert obj.reward.key['first'] == 'something'


def test_non_dict_obj_like_dict():
    obj = DictO(dict_with_obj)
    assert obj['reward.key'] == {'first': 'something'}


def test_non_dict_obj_exception():
    obj = DictO(dict_with_obj)
    with pytest.raises(AttributeError):
        del(obj.reward.key.first)


def test_cast_type_list():
    obj = DictO(dict_with_obj, [DictOInherit])
    assert obj.__get_cast_type__() == [DictOInherit, dict]


def test_cast_type_list2():
    obj = DictO(dict_with_obj, DictOInherit)
    assert obj.__get_cast_type__() == [DictOInherit, dict]


def test_cast_type():
    obj = DictO(dict_with_obj, DictOInherit)
    assert type(obj.players[0]) == DictO
