# DictO

Use a dict like an object, or just get a value with its path in the dict

```python3
>>> from dicto import DictO
>>> d = {'a': 1, 'b': {'c': 2}, 'd': ["hi", {'foo': "bar"}]}
>>> c = DictO(d)
>>> c['a']
1
>>> c.a
1
>>> c.b
{'c': 2}
>>> c.b.c
2
>>> c['b.c']
2
>>> c['d.1.foo']
bar
>>> c.e = 5
>>> c.e
5
>>> del(c.d)
>>> c
{'a': 1, 'b': {'c': 2}, 'e': 5}
```
