#!/usr/bin/env python3

from glob import glob
from os.path import basename
from os.path import splitext

from setuptools import find_packages
from setuptools import setup

def readme():
    with open('README.md') as f:
        return f.read()

setup(name='DictO',
      version='0.5.2',
      description='A class to enhance dict with object attributes',
      long_description=readme(),
      url='https://gitlab.com/davidblaisonneau-orange/dicto',
      author='David Blaisonneau - Orange OpenSource',
      license='Apache 2.0',
      packages=find_packages('src'),
      package_dir={'': 'src'},
      py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
      include_package_data=True,
      install_requires=[
      ],
      setup_requires=["pytest-runner"],
      tests_require=[
        "pytest",
        "pytest-cov",],
      zip_safe=False)
